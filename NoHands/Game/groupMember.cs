﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoHands.Game
{
    class groupMember
    {
        public String playerName;
        public int currentHP;
        public String playerRole;
        public int posX;
        public int posY;
        public int groupSlot;

        public groupMember(String playername, int currenthp, String playerrole, int x, int y, int groupslot)
        {
            playerName = playername;
            currentHP = currenthp;
            playerRole = playerrole;
            posX = x;
            posY = y;
            groupSlot = groupslot;
        }

        public void updateHP(int newhp)
        {
            currentHP = newhp;
        }
    }
}
