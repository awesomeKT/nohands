﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;
using NoHands;

namespace NoHands
{
    public partial class Form1 : Form
    {
        public Size s;
        public Size hp;
        public Size name;
        public Image srcimg;
        private Sense.visionData vision = new Sense.visionData();
        private Sense.dataManager datamanager = new Sense.dataManager();
        public Timer t1 = new Timer();

        public Form1()
        {
            InitializeComponent();

            
            t1.Interval = 50;
            t1.Tick += new EventHandler(timer1_Tick);

            s.Height = 40;
            s.Width = 120;

            hp.Height = 22;
            hp.Width = 89;

            name.Height = 43;
            name.Width = 163;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            textBox1.Text = Cursor.Position.X.ToString();
            textBox11.Text = Cursor.Position.Y.ToString();

            srcimg = vision.CaptureScreen(Cursor.Position.X, Cursor.Position.Y, 0, 0, s);
            pictureBox1.Image = srcimg;
           // Bitmap bit = new Bitmap(srcimg);
            //Tuple<string,string> currentvision = vision.processImage(bit);
            //textBox3.Text = currentvision.Item1;
           // textBox2.Text = currentvision.Item2;

            int Ycords = 433;
            for (int i = 0; i < 5; i++)
            {
                srcimg = vision.CaptureScreen(289, Ycords, 0, 0, hp);
                //pictureBox1.Image = srcimg;
                Bitmap bit = new Bitmap(srcimg);
                Tuple<string, string> currentvision = vision.processImage(bit);
                datamanager.Logger(DateTime.Now.ToString("ffffff") + ": Group slot " + i + " HP: " + datamanager.formatPercentage(currentvision.Item2) + "(" + currentvision.Item1 + ")");
                Ycords = Ycords + 58;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            s.Height = Convert.ToInt32(textBox4.Text);
            s.Width =  Convert.ToInt32(textBox5.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox12.Text = Environment.CurrentDirectory.ToString();
            datamanager.truncateLog();
            datamanager.Logger("Session Started: " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss ffffff"));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox6.Text = textBox1.Text + " x " + textBox11.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox7.Text = textBox1.Text + " x " + textBox11.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox9.Text = textBox1.Text + " x " + textBox11.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox8.Text = textBox1.Text + " x " + textBox11.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox10.Text = textBox1.Text + " x " + textBox11.Text;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            datamanager.Logger("Session closed: " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss ffffff"));
       
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int Ycords = 427;
            for (int i = 0; i < 5; i++)
            {
                srcimg = vision.CaptureScreen(79, Ycords, 0, 0, name);
                Bitmap bit = new Bitmap(srcimg);
                Tuple<string, string> currentvision = vision.processImage(bit);
                datamanager.Logger("Group slot " + i + ": " + currentvision.Item2 + "(" + currentvision.Item1 + ")");
                Ycords = Ycords+ 58;
            }
            t1.Enabled = true;
       }
    }
}
