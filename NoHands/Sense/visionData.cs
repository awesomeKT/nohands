﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;
using NoHands.Game;

namespace NoHands.Sense
{
    class visionData
    {
        public Image CaptureScreen(int sourceX, int sourceY, int destX, int destY,
    Size regionSize)
        {
            Bitmap bmp = new Bitmap(regionSize.Width, regionSize.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(sourceX, sourceY, destX, destY, regionSize);
            return bmp;

        }

        public Tuple<string, string> processImage(Bitmap image)
        {
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var pix = PixConverter.ToPix(image))
                {
                    using (var page = engine.Process(pix))
                    {
                        return Tuple.Create(String.Format("{0:P}", page.GetMeanConfidence()), page.GetText());
                    }
                }
            }
        }

        public void addMember(String name, String role, int x, int y)
        {
            //groupMember newMember = new groupMember(name,100,role,x,y);
        }
    }
}
