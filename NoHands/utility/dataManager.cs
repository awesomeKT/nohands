﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace NoHands.Sense
{
    class dataManager
    {
        public string logLocation = Environment.CurrentDirectory.ToString() + @"\log.txt";

        public void truncateLog()
        {
            File.WriteAllText(logLocation, String.Empty);
        }

        public void Logger(String lines)
        {

            // Write the string to a file.append mode is enabled so that the log
            // lines get appended to  test.txt than wiping content and writing the log

            StreamWriter file = new StreamWriter(logLocation, true);
            file.WriteLine(lines);

            file.Close();
        }

        public String formatPercentage(String var)
        {
            return string.Join(null, Regex.Split(var, @"[^.\d]"));
        }
    }
}
